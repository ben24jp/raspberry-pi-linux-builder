FROM ubuntu:22.04

# Update package database
RUN apt-get update

# Set the locale environment variables
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL=en_US.UTF-8

# Build variables
ENV KERNEL=kernel8
ENV ARCH=arm64
ENV CROSS_COMPILE=aarch64-linux-gnu-

RUN apt-get install -y locales && \
locale-gen en_US.UTF-8 && \
update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

# Set non-interactive environment
ENV DEBIAN_FRONTEND=noninteractive

# Install required packages
RUN apt install -y \
git bc bison flex libssl-dev make libc6-dev \
libncurses5-dev crossbuild-essential-arm64
# Create a non-root used
RUN useradd -ms /bin/bash linux

# Switch to the new user
USER linux
WORKDIR /home/linux/source

CMD ./home/linux/commands/build_commands.sh
