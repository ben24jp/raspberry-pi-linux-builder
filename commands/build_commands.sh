#!/bin/bash

PROJECT_DIR="/home/linux/source"
TARGET_DIR="/home/linux/output"

make distclean
make bcm2711_defconfig
make -j 16 Image modules dtbs

cp ${PROJECT_DIR}/arch/arm64/boot/Image ${TARGET_DIR}/fat32/$KERNEL.img
cp ${PROJECT_DIR}/arch/arm64/boot/dts/broadcom/*.dtb ${TARGET_DIR}/fat32/
cp ${PROJECT_DIR}/arch/arm64/boot/dts/overlays/*.dtb* ${TARGET_DIR}/fat32/overlays/
cp ${PROJECT_DIR}/arch/arm64/boot/dts/overlays/README ${TARGET_DIR}/fat32/overlays/
