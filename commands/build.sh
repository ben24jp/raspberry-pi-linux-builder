#!/bin/bash

TARGET_DIR="output"

mkdir -p ${TARGET_DIR}/fat32/overlays
mkdir -p ${TARGET_DIR}/ext4

time docker run \
	--rm \
	-v ~/Documents/Linux/linux:/home/linux/source \
	-v ~/Documents/Linux/commands:/home/linux/commands \
	-v ~/Documents/Linux/output:/home/linux/output \
	--name linux-builder \
	rpi-linux_builder
